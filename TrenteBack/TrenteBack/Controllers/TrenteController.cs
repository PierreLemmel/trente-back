﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrenteBack.Models;
using TrenteBack.Services;
using TrenteBack.ViewModels;

namespace TrenteBack.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TrenteController : ControllerBase
    {
        private readonly IShowService showService;
        private readonly TrenteHub trenteHub;

        public TrenteController(IShowService showService, TrenteHub trenteHub)
        {
            this.showService = showService;
            this.trenteHub = trenteHub;
        }

        [HttpGet("{show}/")]
        public ActionResult<GetShowResponse> Get(string show)
        {
            ShowState state = showService.GetShowState(show);
            ShowViewModel vm = ModelToViewModel(state);
            GetShowResponse response = new(vm);
            
            return Ok(response);
        }

        [HttpPost("{show}/increase-counter")]
        public async Task<ActionResult<IncreaseCounterResponse>> IncreaseCounter(string show, IncreaseCounterRequest request)
        {
            ShowState state = showService.IncreaseCounter(show);
            ShowViewModel vm = ModelToViewModel(state);

            await trenteHub.PushState(show, vm);

            IncreaseCounterResponse response = new(vm);
            return Ok(response);
        }

        [HttpPost("{show}/decrease-counter")]
        public async Task<ActionResult<DecreaseCounterResponse>> DecreaseCounter(string show, DecreaseCounterRequest request)
        {
            ShowState state = showService.DecreaseCounter(show);
            ShowViewModel vm = ModelToViewModel(state);

            await trenteHub.PushState(show, vm);

            DecreaseCounterResponse response = new(vm);
            return Ok(response);
        }

        [HttpPost("{show}/reset-counter")]
        public async Task<ActionResult<ResetCounterResponse>> ResetCounter(string show, ResetCounterRequest request)
        {
            ShowState state = showService.ResetCounter(show);
            ShowViewModel vm = ModelToViewModel(state);

            await trenteHub.PushState(show, vm);

            ResetCounterResponse response = new(vm);
            return Ok(response);
        }

        private ShowViewModel ModelToViewModel(ShowState model) => new(model.CurrentScene);
    }
}