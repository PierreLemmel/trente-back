﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using TrenteBack.Models;
using TrenteBack.ViewModels;

namespace TrenteBack.Controllers
{
    public class TrenteHub : Hub
    {
        private readonly IHubContext<TrenteHub> context;

        public async Task Subscribe(string show) => await context.Groups.AddToGroupAsync(Context.ConnectionId, show);
        public async Task Unsuscribe(string show) => await context.Groups.RemoveFromGroupAsync(Context.ConnectionId, show);

        public async Task PushState(string show, ShowViewModel state) => await context.Clients.Group(show).SendAsync("stateChanged", state);

        public TrenteHub(IHubContext<TrenteHub> context) => this.context = context;
    }
}