﻿using System;

namespace TrenteBack.Models
{
    public record ShowState(int CurrentScene)
    {
        public static ShowState NewShow => new(1);
    }
}