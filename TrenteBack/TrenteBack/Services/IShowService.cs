﻿using TrenteBack.Models;

namespace TrenteBack.Services
{
    public interface IShowService
    {
        ShowState GetShowState(string show);

        ShowState IncreaseCounter(string show);
        ShowState DecreaseCounter(string show);
        ShowState ResetCounter(string show);
    }
}