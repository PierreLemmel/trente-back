﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrenteBack.Models;

namespace TrenteBack.Services
{
    public class ShowService : IShowService
    {
        private IDictionary<string, ShowState> shows = new Dictionary<string, ShowState>();

        public ShowState DecreaseCounter(string show)
        {
            ShowState oldState = GetOrCreateDefault(show);
            ShowState newState = oldState with { CurrentScene = oldState.CurrentScene - 1 };

            shows[show] = newState;
            return newState;
        }

        public ShowState GetShowState(string show) => GetOrCreateDefault(show);

        public ShowState IncreaseCounter(string show)
        {
            ShowState oldState = GetOrCreateDefault(show);
            ShowState newState = oldState with { CurrentScene = oldState.CurrentScene + 1 };

            shows[show] = newState;
            return newState;
        }

        public ShowState ResetCounter(string show)
        {
            ShowState newState = ShowState.NewShow;

            shows[show] = newState;
            return newState;
        }

        private ShowState GetOrCreateDefault(string show)
        {
            ShowState? result;

            if (!shows.TryGetValue(show, out result))
            {
                result = ShowState.NewShow;
            }

            return result;
        }
    }
}