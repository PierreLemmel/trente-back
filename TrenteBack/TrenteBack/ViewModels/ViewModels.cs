﻿namespace TrenteBack.ViewModels
{
    public record ShowViewModel(int CurrentScene);

    public record GetShowResponse(ShowViewModel Show);

    public record IncreaseCounterRequest;
    public record IncreaseCounterResponse(ShowViewModel Show);

    public record DecreaseCounterRequest;
    public record DecreaseCounterResponse(ShowViewModel Show);

    public record ResetCounterRequest;
    public record ResetCounterResponse(ShowViewModel Show);
}